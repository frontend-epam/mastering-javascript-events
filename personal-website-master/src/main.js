let externalHTML = `
<section class="app-section join-program">
        <h2 class="app-title">
            Join Our Program
        </h2>
        <h3 class="app-subtitle">
            Sed do eiusmod tempor incididunt<br>
            ut labore et dolore magna aliqua. </h3>
        <form class="join-program__form" id="join-program__form">
            <input type="email" class="join-program__input" id="join-program__input" value="Email">
            <input type="submit" id="join-program__submit-btn" value=" Subscribe">
        </form>
    </section>
`;

let externalStyles = `
.join-program {
    background-image: url(assets/images/photo.png);
    color: #fff;
    padding-top: 57px;
    padding-bottom: 100px;
}

.join-program__form {
    display: flex;
    flex-direction: row;
    width: 45%;
}

.join-program__input {
    background-color: #FFFFFF26;
    color: #fff;
    border: none;
    padding: 10px 20px;
    width: 65%;
    margin-right: 30px;
}

#join-program__submit-btn {
    font-family: 'Oswald', sans-serif;
    font-size: 14px;
    background-color: #55C2D8;
    color: #ffffff;
    text-transform: uppercase;
    border-radius: 18px;
    padding: 6px 16px;
    border: none;
}

@media only screen and (max-width: 768px) {
    .join-program {
        background-size: cover;
    }
    .join-program__form {
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 100%;
        gap: 42px;
    }
    .join-program__input {
        margin-right: 0;
    }

    #join-program__submit-btn {
        width: 25%;
    }
}

`;

function insertExternalHTML() {
    var container = document.getElementById("app-container");
    var footer = document.querySelector('.app-footer');
    footer.insertAdjacentHTML('beforebegin', externalHTML);

    // Append the external styles to the <head> of the document
    var styleElement = document.createElement('style');
    styleElement.textContent = externalStyles;
    document.head.appendChild(styleElement);

    let form = document.getElementById("join-program__form");
    form.addEventListener('submit', function (event) {
        // Prevent the default form behavior
        event.preventDefault();

        // Get the value entered in the email input field
        var emailInput = document.getElementById('join-program__input');
        var enteredValue = emailInput.value;

        // Log the entered value to the console
        console.log('Entered email value:', enteredValue);
    });
}

// Call the function to insert the external markup when the page loads
document.addEventListener('DOMContentLoaded', function () {
    insertExternalHTML();
});

